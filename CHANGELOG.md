# CHANGELOG
 - [v0.1.0](#v010)
 - [v0.1.1](#v011)

* * *


### v0.1.1
- As of Sept. 10, 2014.
- Enchance the Assets Importer
- Now using `Walisph` Namespace
- Rollback to psr-0 instead of psr-4
- Major Support Walisph 2 Applications
- With PHPSpec and PHPUnit testing
- Building with Travis
### v0.1.0
- The first stable with cleaner and importer
